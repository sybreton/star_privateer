Installation
============

The simplest way to install the module is through PyPi

``pip install star-privateer``

The module is also available through its GitLab repository. You have to
clone first:

``git clone https://gitlab.com/sybreton/star_privateer.git``

then go to the local repository and simply do

``pip install .``

Some of the tutoriels notebook require additional datasets to be
properly run, you can access them through an auxiliary repository

``git clone https://gitlab.com/sybreton/plato_msap4_demonstrator_datasets.git``

that you will also have to install through

``pip install .``

In the future, we plan to provide packaged versions of the pipeline
through PyPi and conda-forge.

