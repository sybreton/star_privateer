Citing *star-privateer*
#######################

If you use *star-privateer* in one of your publications, please cite
the following paper, `Breton et al. (2024)
<https://ui.adsabs.harvard.edu/abs/2024arXiv240703709B/abstract>`_.

.. parsed-literal::

  @ARTICLE{2024A&A...689A.229B,
         author = {{Breton}, S.~N. and {Lanza}, A.~F. and {Messina}, S. and {Pagano}, I. and {Bugnet}, L. and {Corsaro}, E. and {Garc{\'\i}a}, R.~A. and {Mathur}, S. and {Santos}, A.~R.~G. and {Aigrain}, S. and {Amard}, L. and {Brun}, A.~S. and {Degott}, L. and {Noraz}, Q. and {Palakkatharappil}, D.~B. and {Panetier}, E. and {Strugarek}, A. and {Belkacem}, K. and {Goupil}, M. -J. and {Ouazzani}, R.~M. and {Philidet}, J. and {Reni{\'e}}, C. and {Roth}, O.},
          title = "{Measuring stellar surface rotation and activity with the PLATO mission: I. Strategy and application to simulated light curves}",
        journal = {\aap},
       keywords = {methods: data analysis, stars: low-mass, stars: rotation, stars: solar-type, starspots, Astrophysics - Solar and Stellar Astrophysics, Astrophysics - Earth and Planetary Astrophysics, Astrophysics - Instrumentation and Methods for Astrophysics},
           year = 2024,
          month = sep,
         volume = {689},
            eid = {A229},
          pages = {A229},
            doi = {10.1051/0004-6361/202449893},
  archivePrefix = {arXiv},
         eprint = {2407.03709},
   primaryClass = {astro-ph.SR},
         adsurl = {https://ui.adsabs.harvard.edu/abs/2024A&A...689A.229B},
        adsnote = {Provided by the SAO/NASA Astrophysics Data System}
  }




