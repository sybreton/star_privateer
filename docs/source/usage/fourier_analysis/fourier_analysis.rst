Fourier analysis (MSAP4-01A)
============================

.. code:: ipython3

    import star_privateer as sp
    import plato_msap4_demonstrator_datasets.plato_sim_dataset as plato_sim_dataset

.. code:: ipython3

    import numpy as np
    import matplotlib.pyplot as plt
    import pandas as pd

.. code:: ipython3

    sp.__version__




.. parsed-literal::

    '1.2.0'



K2: Rotation period analysis
----------------------------

.. code:: ipython3

    t, s, dt = sp.load_k2_example ()

.. code:: ipython3

    fig, ax = plt.subplots (1, 1, figsize=(8,4))
    
    ax.scatter (t[s!=0]-t[0], s[s!=0], color='black', 
                marker='o', s=1)
    
    ax.set_xlabel ('Time (day)')
    ax.set_ylabel ('Flux (ppm)')
    
    fig.tight_layout ()



.. image:: fourier_analysis_files/fourier_analysis_6_0.png


As we want to recover rotation periods below 45 days, we only consider
the section of the periodogram verifying
:math:`P < P_\mathrm{cutoff} = 60` days.

.. code:: ipython3

    pcutoff = 60

As a preprocessing step, we compute the Lomb-Scargle periodogram (in the
SAS framework, it will be directyly provided by MSAP1).

.. code:: ipython3

    p_ps, ls = sp.compute_lomb_scargle (t, s)

Now we perform the periodogram analysis.

.. code:: ipython3

    cond = p_ps < pcutoff
    (prot, e_p, E_p, 
     _, param, h_ps) = sp.compute_prot_err_gaussian_fit_chi2_distribution (p_ps[cond], ls[cond], pfa_threshold=1e-6, 
                                                                           plot_procedure=False,
                                                                           verbose=False)
    fig= sp.plot_ls (p_ps, ls, filename='figures/fourier_k2.png', param_profile=param, 
                     logscale=False, xlim=(0.1, 5))



.. image:: fourier_analysis_files/fourier_analysis_12_0.png


.. code:: ipython3

    IDP_SAS_PROT_FOURIER = sp.prepare_idp_fourier (param, h_ps, ls.size,
                                                  pcutoff=pcutoff, pthresh=None,
                                                  pfacutoff=1e-6)
    
    df = pd.DataFrame (data=IDP_SAS_PROT_FOURIER)
    df




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>0</th>
          <th>1</th>
          <th>2</th>
          <th>3</th>
          <th>4</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>2.800785</td>
          <td>0.002798</td>
          <td>0.002804</td>
          <td>408.999297</td>
          <td>1.000000e-16</td>
        </tr>
        <tr>
          <th>1</th>
          <td>1.400584</td>
          <td>0.001399</td>
          <td>0.001402</td>
          <td>307.750010</td>
          <td>1.000000e-16</td>
        </tr>
        <tr>
          <th>2</th>
          <td>0.781536</td>
          <td>0.000781</td>
          <td>0.000782</td>
          <td>237.424642</td>
          <td>1.000000e-16</td>
        </tr>
        <tr>
          <th>3</th>
          <td>1.371679</td>
          <td>0.001370</td>
          <td>0.001373</td>
          <td>228.878140</td>
          <td>1.000000e-16</td>
        </tr>
        <tr>
          <th>4</th>
          <td>2.688480</td>
          <td>0.002685</td>
          <td>0.002691</td>
          <td>179.810535</td>
          <td>1.000000e-16</td>
        </tr>
        <tr>
          <th>5</th>
          <td>1.429956</td>
          <td>0.001428</td>
          <td>0.001431</td>
          <td>63.878992</td>
          <td>1.000000e-16</td>
        </tr>
        <tr>
          <th>6</th>
          <td>0.127013</td>
          <td>0.000009</td>
          <td>0.000009</td>
          <td>41.450787</td>
          <td>1.000000e-16</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code:: ipython3

    df.to_latex (buf='data_products/idp_sas_prot_fourier_k2_211015853.tex', 
                 formatters=['{:.2f}'.format, '{:.2f}'.format, '{:.2f}'.format,
                             '{:.2f}'.format, '{:.0e}'.format],  
                 index=False, header=False)
    np.savetxt ('data_products/IDP_SAS_PROT_FOURIER_K2.dat', 
                 IDP_SAS_PROT_FOURIER)

PLATO: Rotation period analysis
-------------------------------

The PLATO simulation below encompasses both rotational modulation and
low-frequency modulations due to activity. In order to analyse the
rotational signal, we first filter out frequencies above 60 days (in
PLATO, this will be done outside MSAP4).

.. code:: ipython3

    filename = sp.get_target_filename (plato_sim_dataset, '040', filetype='csv')
    t, s, dt = sp.load_resource (filename)
    s_filtered = sp.preprocess (t, s, cut=60)

.. code:: ipython3

    fig, ax = plt.subplots (1, 1, figsize=(8,4))
    
    ax.scatter (t[s!=0]-t[0], s[s!=0], color='black', 
                marker='o', s=1, label="Unfiltered")
    ax.scatter (t[s!=0]-t[0], s_filtered[s_filtered!=0], color='darkorange', 
                marker='o', s=1, label="Filtered")
    
    ax.set_xlabel ('Time (day)')
    ax.set_ylabel ('Flux (ppm)')
    
    ax.legend ()
    
    fig.tight_layout ()



.. image:: fourier_analysis_files/fourier_analysis_18_0.png


As we want to recover rotation periods below 60 days, we only consider
the section of the periodogram verifying
:math:`P < P_\mathrm{cutoff} = 60` days.

.. code:: ipython3

    pcutoff = 60

As a preprocessing step, we compute the Lomb-Scargle periodogram (in the
SAS framework, it will be directyly provided by MSAP1).

.. code:: ipython3

    p_ps, ls = sp.compute_lomb_scargle (t, s_filtered)

Now we perform the periodogram analysis.

.. code:: ipython3

    cond = p_ps < pcutoff
    (prot, e_p, E_p, 
     _, param, h_ps) = sp.compute_prot_err_gaussian_fit_chi2_distribution (p_ps[cond], 
                                                                           ls[cond], 
                                                                           pfa_threshold=1e-6,
                                                                           verbose=False)
    sp.plot_ls (p_ps, ls, filename='figures/fourier_plato_short.png', param_profile=param, 
                logscale=False, xlim=(1, pcutoff), 
                )
    IDP_SAS_PROT_FOURIER = sp.prepare_idp_fourier (param, h_ps, ls.size,
                                                  pcutoff=pcutoff, pthresh=None,
                                                  pfacutoff=1e-6)
    df = pd.DataFrame (data=IDP_SAS_PROT_FOURIER)
    df




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>0</th>
          <th>1</th>
          <th>2</th>
          <th>3</th>
          <th>4</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>25.433099</td>
          <td>0.025403</td>
          <td>0.025454</td>
          <td>44.875420</td>
          <td>1.000000e-16</td>
        </tr>
        <tr>
          <th>1</th>
          <td>6.076207</td>
          <td>0.006068</td>
          <td>0.006080</td>
          <td>15.077311</td>
          <td>2.831438e-07</td>
        </tr>
      </tbody>
    </table>
    </div>




.. image:: fourier_analysis_files/fourier_analysis_24_1.png


.. code:: ipython3

    df.to_latex (buf='data_products/idp_sas_prot_fourier_plato_040.tex', 
                 formatters=['{:.2f}'.format, '{:.2f}'.format, '{:.2f}'.format,
                             '{:.2f}'.format, '{:.0e}'.format],  
                 index=False, header=False)
    np.savetxt ('data_products/IDP_SAS_PROT_FOURIER_PLATO.dat', 
                 IDP_SAS_PROT_FOURIER)

PLATO: Long term modulation analysis
------------------------------------

This time, we are interested in recovering long term modulations. We
consider the section of the periodogram verifying
:math:`P > P_\mathrm{tresh} = 60` days.

.. code:: ipython3

    pthresh = 60

As a preprocessing step, we compute the Lomb-Scargle periodogram (in the
SAS framework, it will be directyly provided by MSAP1).

.. code:: ipython3

    p_ps, ls = sp.compute_lomb_scargle (t, s, normalisation="snr_flat")

Now we perform the periodogram analysis.

.. code:: ipython3

    (plongterm, e_p, E_p, 
     _, param, h_ps) = sp.compute_prot_err_gaussian_fit_chi2_distribution (p_ps[p_ps>pthresh], 
                                                                           ls[p_ps>pthresh], 
                                                                           pfa_threshold=1e-6, 
                                                                           verbose=False)
    fig = sp.plot_ls (p_ps, ls, filename='figures/fourier_plato_long.png', param_profile=param, 
                        logscale=False, xlim=(1,8*pthresh))
    IDP_SAS_LONGTERM_MODULATION_FOURIER = sp.prepare_idp_fourier (param, h_ps, ls.size,
                                                                  pcutoff=None, pthresh=pthresh,
                                                                  pfacutoff=1e-6)
    df = pd.DataFrame (data=IDP_SAS_LONGTERM_MODULATION_FOURIER)
    df




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>0</th>
          <th>1</th>
          <th>2</th>
          <th>3</th>
          <th>4</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>343.497833</td>
          <td>0.343238</td>
          <td>0.343925</td>
          <td>8.576472e+06</td>
          <td>1.000000e-16</td>
        </tr>
        <tr>
          <th>1</th>
          <td>685.994492</td>
          <td>0.684484</td>
          <td>0.685852</td>
          <td>2.198985e+06</td>
          <td>1.000000e-16</td>
        </tr>
        <tr>
          <th>2</th>
          <td>85.690887</td>
          <td>0.085443</td>
          <td>0.085613</td>
          <td>4.172111e+05</td>
          <td>1.000000e-16</td>
        </tr>
        <tr>
          <th>3</th>
          <td>114.308839</td>
          <td>0.114031</td>
          <td>0.114259</td>
          <td>4.035625e+05</td>
          <td>1.000000e-16</td>
        </tr>
        <tr>
          <th>4</th>
          <td>62.343966</td>
          <td>0.062187</td>
          <td>0.062311</td>
          <td>3.141029e+05</td>
          <td>1.000000e-16</td>
        </tr>
        <tr>
          <th>5</th>
          <td>228.516929</td>
          <td>0.227862</td>
          <td>0.228317</td>
          <td>1.820337e+05</td>
          <td>1.000000e-16</td>
        </tr>
        <tr>
          <th>6</th>
          <td>97.934998</td>
          <td>0.097654</td>
          <td>0.097849</td>
          <td>1.662900e+05</td>
          <td>1.000000e-16</td>
        </tr>
        <tr>
          <th>7</th>
          <td>171.403827</td>
          <td>0.170925</td>
          <td>0.171267</td>
          <td>1.490459e+05</td>
          <td>1.000000e-16</td>
        </tr>
        <tr>
          <th>8</th>
          <td>76.173568</td>
          <td>0.075956</td>
          <td>0.076108</td>
          <td>1.486087e+05</td>
          <td>1.000000e-16</td>
        </tr>
        <tr>
          <th>9</th>
          <td>137.128087</td>
          <td>0.136749</td>
          <td>0.137022</td>
          <td>8.018874e+04</td>
          <td>1.000000e-16</td>
        </tr>
      </tbody>
    </table>
    </div>




.. image:: fourier_analysis_files/fourier_analysis_32_1.png


.. code:: ipython3

    df.to_latex (buf='data_products/idp_sas_longterm_modulation_fourier_plato_040.tex', 
                 formatters=['{:.2f}'.format, '{:.2f}'.format, '{:.2f}'.format,
                             '{:.2f}'.format, '{:.0e}'.format],  
                 index=False, header=False)
    np.savetxt ('data_products/IDP_SAS_LONGTERM_MODULATION_FOURIER_PLATO.dat', 
                 IDP_SAS_LONGTERM_MODULATION_FOURIER)

